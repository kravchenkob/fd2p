//
//  AccountVC.swift
//  FD2P
//
//  Created by Bogdan Kravchenko on 10/26/18.
//  Copyright © 2018 Kravchenko Group. All rights reserved.
//

import UIKit

class AccountVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let editButton = UIBarButtonItem (barButtonSystemItem:.edit, target: self, action: #selector(editProfile))
        self.navigationItem.rightBarButtonItem = editButton
    }
    
    @objc func editProfile(){
        print("Edit Profile")
        
    }
}
